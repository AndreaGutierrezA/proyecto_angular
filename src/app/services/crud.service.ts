import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { ITask } from '../interfaces/crud.type';

@Injectable({
  providedIn: 'root'
})
export class CrudService {

  constructor(private http: HttpClient) {
  }

  async getListTasks(): Promise<ITask[]> {
    return fetch(`${environment.url}/workflows/${environment.workflowId}/tasks`, {
      method: 'GET',
      headers: {
        'Authorization': 'Bearer eaaq5mRb4cSmdmRUTJrbr1ZO',
        'Content-Type': 'application/json'
      }
    }).then(res => res.json() )
  }

  async addTask(task: ITask) {
    return fetch(`${environment.url}/workflows/${environment.workflowId}/tasks`, {
      method: 'POST',
      body: JSON.stringify(task), 
      headers: {
        'Authorization': 'Bearer eaaq5mRb4cSmdmRUTJrbr1ZO',
        'Content-Type': 'application/json'
      }
    }).then(res => res.json())
  }

  async modifyTask(task: ITask) {
    return fetch(`${environment.url}/workflows/${environment.workflowId}/tasks`, {
      method: 'PUT',
      body: JSON.stringify(task), 
      headers: {
        'Authorization': 'Bearer eaaq5mRb4cSmdmRUTJrbr1ZO',
        'Content-Type': 'application/json'
      }
    }).then(res => res.json())
  }

  // prueba exitosa en postman https://app.flowdash.com/api/v1/workflows/eEIpkD/tasks/27
  // pendiente que elimine 
  async deleteTask(task: ITask) {
    return fetch(`${environment.url}/workflows/${environment.workflowId}/tasks/${task.id}`, {
      method: 'DELETE',
      headers: {
        'Authorization': 'Bearer eaaq5mRb4cSmdmRUTJrbr1ZO',
        'Content-Type': 'application/json'
      }
    }).then(res => res.json())
  }
}

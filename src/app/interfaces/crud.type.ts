export interface ITask{
    Stage: string,
    AssignedTo: string,
    CreatedOn: string,
    UpdatedOn: string,
    TaskURL: string,
    id: string,
    Name: string
}

export interface IDialogData {
    task: ITask | null;
    processData: Function;
  }

export interface IDialogDelete {
    task: ITask | null;
    deleteTask: Function;
  }
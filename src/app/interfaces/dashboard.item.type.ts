export interface DashboardItem {
  theme: string;
  themeIcon: string;
  campaignTitle: string;
  campaignType: string;
  status: string;
  impactedUsers: number;
  color: string;
  creationDate: Date;
  sendDate: Date;
  expandableData: Array<{
    icon: string;
    label: string;
    value: string
  }>;
}
export interface IDashboardTask{
  theme: string;
  themeIcon: string;
  color: string;
  stats : {
    label: string;
    value: number;
  }[];

}

export interface ITask{
  Stage: string,
  AssignedTo: string,
  CreatedOn: string,
  UpdatedOn: string,
  TaskURL: string,
  id: string,
  Name: string
}

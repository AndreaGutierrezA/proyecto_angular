import { Component, Injectable, OnInit } from "@angular/core";
import { DashboardItem, IDashboardTask, ITask } from "../../interfaces/dashboard.item.type";
import { DashboardService } from "../../services/dashboard.service";
import { ToastService } from "../../services/toast.service";
const stages = ['Not Started', 'In Progress', 'Done'];
const users = ['dmaria0990@gmail.com', 'hexapodo@gmail.com',]
@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.component.html",
  styleUrls: ["./dashboard.component.scss"],
})
@Injectable({
  providedIn: "root",
})
export class DashboardComponent implements OnInit {
  public elements: ITask[] = [];
  public stageOverview: IDashboardTask | undefined;
  public userOverview: IDashboardTask | undefined;
  public userCount = new Object();

  public loading = false;
  public ngxLoadingAnimationTypes = {
    chasingDots: "chasing-dots",
    circle: "sk-circle",
    circleSwish: "circleSwish",
    cubeGrid: "sk-cube-grid",
    doubleBounce: "double-bounce",
    none: "none",
    pulse: "pulse",
    rectangleBounce: "rectangle-bounce",
    rotatingPlane: "rotating-plane",
    threeBounce: "three-bounce",
    wanderingCubes: "wandering-cubes",
  };

  constructor(
    private dashboardService: DashboardService,
    private toast: ToastService
  ) {
    const stats = stages.map(
      (stage) => {
        return {
          label: stage,
          value: 0
        }
      }
    )

    this.stageOverview = {
      stats,
      theme: "Stages Overview",
      color: "#65BE51",
      themeIcon: "nutricion@PYPGrandes",

    }
    const usersList = users.map(
      (user) => {
        return {
          label: user,
          value: 0
        }
      }
    )
    this.userOverview = {
      stats: usersList,
      theme: "Users OverView",
      color: "yellow",
      themeIcon: "nutricion@PYPGrandes",

    }

    

  }


  public ngOnInit() {
    this.getData().then();
   
  }
  /**
   * getMetrics
   */
  public async getData() {
    try {
      this.loading = true;
      this.elements = await this.dashboardService.getListTasks();
      this.elements.forEach(
        (element) =>  {
          console.log(element);
          console.log(element['Assigned To']);
          // realizar el conteo de cuantos correo hay, saber cuantos pertenecen a cada categoria
          this.userCount[element['Assigned To']] ? this.userCount[element['Assigned To']]: 0 + 1;
        }
      )
      console.log(this.userCount);
      this.loading = false;
      console.log(this.elements);
    } catch (e) {
      console.log(e);
      this.toast.error(
        "No se pudieron obtener los indicadores del dashboard, revise su conexión"
      );
    }
  }
}

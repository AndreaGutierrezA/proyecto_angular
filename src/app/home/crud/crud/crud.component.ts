import { Component, OnInit } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormularioComponent } from '../formulario/formulario.component';
import { DashboardService } from '../../../services/dashboard.service';
import { CrudService } from '../../../services/crud.service';
import { ITask } from 'src/app/interfaces/crud.type';
import { DeleteConfirmationComponent } from './delete-confirmation/delete-confirmation.component';
import { ToastService } from 'src/app/services/toast.service';

@Component({
  selector: 'app-crud',
  templateUrl: './crud.component.html',
  styleUrls: ['./crud.component.scss']
})
export class CrudComponent implements OnInit {

  tasks: ITask[] = [];
  dialogDelete;
  dialogAddAndModify;

  processData = (task: ITask | null) => {
    console.log(task);
    if (task) {
      if (task?.id) {
        this.crudService.modifyTask(task).then();
        this.getAll();
        this.toastService.success(
          "Registro actualizado"
        );
      } else {
        this.crudService.addTask(task).then();
        this.getAll();
        this.toastService.success(
          "Registro ingresado"
        );
      }
    }
    this.dialogAddAndModify.close();
  }
  deleteTask = (deleteTask: boolean, task: ITask) => {
    console.log(task);
    if (deleteTask) {
      this.crudService.deleteTask(task).then();
      this.getAll();
      this.toastService.success(
        "Registro eliminado"
      );
    }
    this.dialogDelete.close();
  }
  constructor(
    public dialog: MatDialog,
    public crudService: CrudService,
    private dashboardService: DashboardService,
    private toastService: ToastService,
  ) { }

  ngOnInit(): void {
    this.getAll();
  }
  getAll() {
    this.crudService.getListTasks().then(
      (tasks) => {
        this.tasks = tasks;
        console.log(this.tasks);
      }
    );
  }

  onCreate() {
    this.dialogAddAndModify = this.dialog.open(FormularioComponent, {
      data: {
        task: null,
        processData: this.processData
      },
      panelClass: 'custom-container'
    });
  }


  onEdit(task: ITask) {
    this.dialogAddAndModify = this.dialog.open(FormularioComponent, {
      data: {
        task: task,
        processData: this.processData
      },
      panelClass: 'custom-container'
    });
  }

  confirmarBorrar(task: ITask): void {
    this.dialogDelete = this.dialog.open(DeleteConfirmationComponent, {
      width: '530px',
      data: {
        task: task,
        title: 'Danger zone',
        deleteTask: this.deleteTask
      },
      panelClass: 'custom-container',
      disableClose: true
    });

    this.dialogDelete.afterClosed().subscribe(result => {
      console.log(result);
      /*  if (result) {
         this.crudService.deleteTask(result.id).subscribe(
           () => {
           },
           (error) => {
             console.error(error);
           }
         );
       } */
    });
  }

  onDelete(task: ITask) {
    console.log(task);
    this.confirmarBorrar(task);
  }


}


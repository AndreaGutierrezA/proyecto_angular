import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { IDialogDelete, ITask } from 'src/app/interfaces/crud.type';

@Component({
  selector: 'app-delete-confirmation',
  templateUrl: './delete-confirmation.component.html',
  styleUrls: ['./delete-confirmation.component.scss']
})
export class DeleteConfirmationComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA)
    public data: IDialogDelete,
  ) { }

  ngOnInit(): void {
    this.data
  }

  onConfirmarDelete(deleteTask: boolean, task: ITask) {
    this.data.deleteTask(deleteTask, task)
  }

}

import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { IDialogData } from 'src/app/interfaces/crud.type';
import { ToastService } from 'src/app/services/toast.service';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.scss']
})
export class FormularioComponent implements OnInit {

  validateForm: FormGroup;
  users: string[] = ['dmaria0990@gmail.com','hexapodo@gmail.com']

  constructor(
    @Inject(MAT_DIALOG_DATA)
    public data: IDialogData,
    private fb: FormBuilder,
  ) { }

  ngOnInit(): void {
    console.log(this.data.task);
    this.validateForm = this.fb.group({
      id: [this.data.task?.id ? this.data.task?.id: null, []],
      'Assigned To': [this.data.task ? this.data.task['Assigned To']: this.users[0], [Validators.required]],
      Name: [this.data.task ? this.data.task.Name: '', [Validators.required]],
      Stage: [this.data.task ? this.data.task.Stage: null, [Validators.required]],
    });
  }

  submit() {
    this.data.processData( this.validateForm.value);
   /*  this.toastService.success(
      "Registro ingresado exitosamente"
    ); */
  }

  onCancel(){
    this.data.processData(null);
  }

}



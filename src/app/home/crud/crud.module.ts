import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatTableModule } from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatDialogModule} from '@angular/material/dialog';
import {MatIconModule} from '@angular/material/icon';

import { CrudRoutingModule } from './crud-routing.module';
import { CrudComponent } from './crud/crud.component';
import { FormularioComponent } from './formulario/formulario.component';
import { TableComponent } from './crud/table/table.component';
import { ReactiveFormsModule } from '@angular/forms';
import { DeleteConfirmationComponent } from './crud/delete-confirmation/delete-confirmation.component';


@NgModule({
  declarations: [CrudComponent, FormularioComponent, TableComponent, DeleteConfirmationComponent],
  imports: [
    CommonModule,
    CrudRoutingModule,
    MatTableModule,
    MatPaginatorModule,
    MatDialogModule,
    MatIconModule,
    ReactiveFormsModule
  ]
})
export class CrudModule { }
